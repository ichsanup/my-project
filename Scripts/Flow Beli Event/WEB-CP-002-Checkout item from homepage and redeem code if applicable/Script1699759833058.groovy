import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://demo-app.site/')

WebUI.click(findTestObject('Object Repository/Flow Beli Event/WEB-CP-002-Checkout item from homepage/Page_Be a Profressional Talent with Coding.ID/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/Flow Beli Event/WEB-CP-002-Checkout item from homepage/Page_Masuk untuk dapatkan akses di Coding.ID/input_Email_email'),
	'ichsanup@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Flow Beli Event/WEB-CP-002-Checkout item from homepage/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'),
	'8SQVv/p9jVTHLrggi8kCzw==')

WebUI.sendKeys(findTestObject('Object Repository/Flow Beli Event/WEB-CP-002-Checkout item from homepage/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'),
	Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Flow Beli Event/WEB-CP-002-Checkout item from homepage/Page_Be a Profressional Talent with Coding.ID/h6_Day 3 Predict using Machine Learning'))

WebUI.click(findTestObject('Object Repository/Flow Beli Event/WEB-CP-002-Checkout item from homepage/Page_Day 3 Predict using Machine Learning - Ziyad/a_Beli Tiket'))

WebUI.click(findTestObject('Object Repository/Flow Beli Event/WEB-CP-002-Checkout item from homepage/Page_Day 3 Predict using Machine Learning - Ziyad/a_Lihat                Pembelian Saya'))

WebUI.click(findTestObject('Object Repository/Flow Beli Event/WEB-CP-002-Checkout item from homepage/Page_Coding.ID - Cart/button_Checkout'))

WebUI.click(findTestObject('Object Repository/Flow Beli Event/WEB-CP-002-Checkout item from homepage/Page_Coding.ID - Cart/input_Total Pembayaran_payment_method'))

WebUI.click(findTestObject('Object Repository/Flow Beli Event/WEB-CP-002-Checkout item from homepage/Page_Coding.ID - Cart/button_Confirm'))

WebUI.click(findTestObject('Object Repository/Flow Beli Event/WEB-CP-002-Checkout item from homepage/Page_Coding.ID - Cart/a_ShopeePay'))

WebUI.closeBrowser()
