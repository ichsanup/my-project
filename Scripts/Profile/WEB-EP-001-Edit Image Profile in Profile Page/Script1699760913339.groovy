import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://demo-app.site/')

WebUI.click(findTestObject('Object Repository/Profile/WEB-EP-001-Edit Image Profile in Profile Page/Page_Be a Profressional Talent with Coding.ID/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/Profile/WEB-EP-001-Edit Image Profile in Profile Page/Page_Masuk untuk dapatkan akses di Coding.ID/input_Email_email'),
	'ichsanup@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Profile/WEB-EP-001-Edit Image Profile in Profile Page/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'),
	'8SQVv/p9jVTHLrggi8kCzw==')

WebUI.sendKeys(findTestObject('Object Repository/Profile/WEB-EP-001-Edit Image Profile in Profile Page/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'),
	Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Profile/WEB-EP-001-Edit Image Profile in Profile Page/Page_Be a Profressional Talent with Coding.ID/i_Kontak_fas fa-user-alt'))

WebUI.click(findTestObject('Object Repository/Profile/WEB-EP-001-Edit Image Profile in Profile Page/Page_Be a Profressional Talent with Coding.ID/a_My Account'))

WebUI.click(findTestObject('Object Repository/Profile/WEB-EP-001-Edit Image Profile in Profile Page/Page_Coding.ID - Dashboard/span_Profil'))

WebUI.click(findTestObject('Object Repository/Profile/WEB-EP-001-Edit Image Profile in Profile Page/Page_Coding.ID - Dashboard/a_Edit Profile'))

WebUI.click(findTestObject('Verify Public/img_Invoice_avatar-icon'))

WebUI.uploadFile(findTestObject('Verify Public/upload_image'), 'C:\\Users\\ichsa\\Downloads\\20170101010100_IMG_2162_11zon.jpg')

WebUI.click(findTestObject('Object Repository/Profile/WEB-EP-001-Edit Image Profile in Profile Page/Page_Coding.ID - Dashboard/button_Save Changes'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Profile/WEB-EP-001-Edit Image Profile in Profile Page/Page_Coding.ID - Dashboard/div_123456 telah di edit'))

WebUI.click(findTestObject('Object Repository/Profile/WEB-EP-001-Edit Image Profile in Profile Page/Page_Coding.ID - Dashboard/button_OK'))

WebUI.closeBrowser()
