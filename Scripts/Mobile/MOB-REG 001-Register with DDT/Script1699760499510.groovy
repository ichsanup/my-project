import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\ichsa\\OneDrive - student.gunadarma.ac.id\\Dokumen\\Bootcamp\\DemoAppV2.apk', true)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 001-Register using valid data/1-button - Login Here'), 0)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 001-Register using valid data/2-TextView - Register, now'), 0)

Mobile.setText(findTestObject('Mobile/Register Mobile/MOB-REG 001-Register using valid data/3-input nama'), Nama, 0)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 004-Register use number for name form/4-click date widget'), Tanggal_lahir)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 001-Register using valid data/5-click ok'), 0)

Mobile.setText(findTestObject('Mobile/Item Verify/verify - contohmail.com'), E_Mail, 0)

Mobile.setText(findTestObject('Mobile/Register Mobile/MOB-REG 001-Register using valid data/7-input whatsapp'), Whatsapp,
	0)

Mobile.setEncryptedText(findTestObject('Mobile/Register Mobile/MOB-REG 001-Register using valid data/8-input password'),
	Kata_Sandi, 0)

Mobile.setEncryptedText(findTestObject('Mobile/Register Mobile/MOB-REG 001-Register using valid data/9-input konfirmasi password'),
	Konfirmasi_kata_sandi, 0)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 001-Register using valid data/10-button - CheckBox'), 0)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 001-Register using valid data/11-button - Daftar'), 0)

Mobile.verifyElementVisible(findTestObject('Mobile/Register Mobile/MOB-REG 001-Register using valid data/12-textview - Not Recieve the email'),
	0)

Mobile.closeApplication()
