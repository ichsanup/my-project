import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\ichsa\\OneDrive - student.gunadarma.ac.id\\Dokumen\\Bootcamp\\DemoAppV2.apk', true)

Mobile.tap(findTestObject('Mobile/Login Mobile/Mob-Log005-Login with empty Password/1-button - Login Here'), 0)

Mobile.setText(findTestObject('Mobile/Login Mobile/Mob-Log005-Login with empty Password/2-input email'), 'ichsanup@gmail.com',
	0)

Mobile.tap(findTestObject('Mobile/Login Mobile/Mob-Log005-Login with empty Password/4-button - login'), 0)

Mobile.verifyElementVisible(findTestObject('Mobile/Item Verify/verify- Invalid Credential'), 0)

Mobile.closeApplication()
