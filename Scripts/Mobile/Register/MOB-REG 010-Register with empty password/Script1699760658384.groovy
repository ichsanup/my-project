import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\ichsa\\OneDrive - student.gunadarma.ac.id\\Dokumen\\Bootcamp\\DemoAppV2.apk', true)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/1-button - Login Here'), 0)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/2-TextView - Register, now'),
	0)

Mobile.setText(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/3-input nama'), 'Ichsan',
	0)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/4-click date widget'), 0)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/5-click ok'), 0)

Mobile.setText(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/6-input email'), 'ichsanpu@gmail.com',
	0)

Mobile.setText(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/7-input whatsapp'), '083807891611',
	0)

Mobile.setEncryptedText(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/8-input password'),
	'8SQVv/p9jVTHLrggi8kCzw==', 0)

Mobile.setEncryptedText(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/8-input password'),
	'', 0)

Mobile.setEncryptedText(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/9-input konfirmasi password'),
	'8SQVv/p9jVTHLrggi8kCzw==', 0)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/10-button - CheckBox'), 0)

Mobile.tap(findTestObject('Mobile/Register Mobile/MOB-REG 010-Register with empty password/11-button - Daftar'), 0)

Mobile.verifyElementVisible(findTestObject('Mobile/Item Verify/verify- Password atleast must contain alphanumeric (a-Z,0-9) with minim 8 characters'),
	0)

Mobile.closeApplication()
