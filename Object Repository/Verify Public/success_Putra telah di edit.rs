<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>success_Putra telah di edit</name>
   <tag></tag>
   <elementGuidId>6bcd9052-b882-46b4-aeae-e97288c73020</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.swal-text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>484a3d73-f015-46cb-bce7-c729c4bc8bf4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>swal-text</value>
      <webElementGuid>1ede058f-b934-4cc0-8e05-3fbb7ca340a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Putra telah di edit </value>
      <webElementGuid>9b4c626b-188e-4747-990f-bc1dcc179983</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;light light-sidebar theme-white&quot;]/div[@class=&quot;swal-overlay swal-overlay--show-modal&quot;]/div[@class=&quot;swal-modal&quot;]/div[@class=&quot;swal-text&quot;]</value>
      <webElementGuid>21b2e8fb-a62b-4473-9580-ab2648bc0c61</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil'])[1]/following::div[1]</value>
      <webElementGuid>f1ff7380-3256-4d3e-9a32-f9d9ed5b9946</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Coding.ID'])[2]/following::div[8]</value>
      <webElementGuid>779bde3a-3349-4ed3-be9c-efefbdf19007</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[1]/preceding::div[1]</value>
      <webElementGuid>6fe9bc31-34e0-41ca-8a0b-9fafdbd54033</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::div[11]</value>
      <webElementGuid>0fc0eda9-08cf-42bd-80f3-f05046db7ae8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Putra telah di edit']/parent::*</value>
      <webElementGuid>ee37a545-b301-4222-a370-e7adda5f55c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]</value>
      <webElementGuid>8f865319-6ef3-4a36-8755-72eb3f49c741</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' Putra telah di edit ' or . = ' Putra telah di edit ')]</value>
      <webElementGuid>5ac265ee-5c4e-405c-9edd-c02da7b92197</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
