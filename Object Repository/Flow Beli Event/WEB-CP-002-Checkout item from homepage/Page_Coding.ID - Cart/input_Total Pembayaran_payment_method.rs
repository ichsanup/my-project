<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Total Pembayaran_payment_method</name>
   <tag></tag>
   <elementGuidId>4cafe3be-0c50-4a87-a9d1-a52baba9602d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#bank</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='bank']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>511db0cc-145c-4ba9-9899-7a9121f88c74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>d92e55a4-0e72-4fef-8681-1cc47d95db7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control-inline payment</value>
      <webElementGuid>814e9598-9ffc-4e6d-b0fc-4f89892270a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>payment_method</value>
      <webElementGuid>baa55d0f-d2a8-41e4-a976-3fbb253c1e3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>e-wallet</value>
      <webElementGuid>6008621f-5d58-460c-a6d4-f3836943a836</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>bank</value>
      <webElementGuid>b3d88c69-df73-4300-8fc4-615d4a9aa1c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bank&quot;)</value>
      <webElementGuid>ba2e62d0-11aa-4af8-a72b-a779953eff6d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='bank']</value>
      <webElementGuid>d19b53b1-d661-451c-97d9-257eefdb5930</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='modalform']/table[2]/tbody/tr/td/input</value>
      <webElementGuid>f7f40757-c806-4211-ab89-b14105a0d9a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/input</value>
      <webElementGuid>346c8e7a-f315-402b-8b2c-7daa24369611</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'payment_method' and @id = 'bank']</value>
      <webElementGuid>8ebd3bb9-c613-4fd3-87a5-52744ea9ba2f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
