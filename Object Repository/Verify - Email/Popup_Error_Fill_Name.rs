<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Popup_Error_Fill_Name</name>
   <tag></tag>
   <elementGuidId>171b2040-50f8-4f52-8a60-954c64ec6ca5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Please fill out this field.' or . = 'Please fill out this field.' )]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[(text() = 'Please fill out this field.' or . = 'Please fill out this field.' )]</value>
      <webElementGuid>0881309d-970e-4043-84d3-1204e10e536c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
