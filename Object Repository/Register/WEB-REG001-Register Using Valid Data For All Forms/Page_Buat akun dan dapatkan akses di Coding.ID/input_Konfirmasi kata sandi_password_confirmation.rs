<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Konfirmasi kata sandi_password_confirmation</name>
   <tag></tag>
   <elementGuidId>d0a92913-b42e-4ae2-b0f0-6f24a6d9a09c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#password-confirm</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='password-confirm']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d853a874-98ee-43e2-b1ae-88ba30982f10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password-confirm</value>
      <webElementGuid>bc20c082-6b6e-4f51-adf3-99e4b960f7ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>293af084-fad9-46b3-8ac4-b70b320148bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Ulangi kata sandi anda</value>
      <webElementGuid>5f016594-339a-49af-b920-b15dc4d8e3ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>e92cdf59-9f51-4b63-b059-3b8a89bf74fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password_confirmation</value>
      <webElementGuid>d6132b74-4152-4b37-81e5-91c45ea75814</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>new-password</value>
      <webElementGuid>6435e438-d987-4ee7-a6a4-7d0a896772c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password-confirm&quot;)</value>
      <webElementGuid>b8d576d9-de08-4413-9332-e4f9af13b6a8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='password-confirm']</value>
      <webElementGuid>588ee101-d8cb-41f5-b193-bf526f920c33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/input</value>
      <webElementGuid>57f19d11-4fca-42a9-a8f7-14be4d21382f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'password-confirm' and @type = 'password' and @placeholder = 'Ulangi kata sandi anda' and @name = 'password_confirmation']</value>
      <webElementGuid>f33bab0b-9821-4949-86bd-fc4311b917f4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
