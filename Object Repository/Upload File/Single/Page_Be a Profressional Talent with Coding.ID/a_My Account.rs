<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_My Account</name>
   <tag></tag>
   <elementGuidId>b27e7228-0ca5-4c78-8bba-b6342b4c9bc5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.wm-dropdown-menu > li:nth-of-type(3) > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[7]/ul/li[3]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e2ae5ae3-120c-4a6a-bb4e-d3224c6d289f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/dashboard</value>
      <webElementGuid>e547fcc0-4c33-4f13-99c7-b0a5241f1ea5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>My Account
                                                                </value>
      <webElementGuid>c400a280-1ad6-457e-b3ed-1ed7f683ef2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;___class_+?26___&quot;]/ul[@class=&quot;wm-dropdown-menu&quot;]/li[3]/a[1]</value>
      <webElementGuid>cab4ed8e-6231-4d24-af24-7e25bc15f5ef</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[7]/ul/li[3]/a</value>
      <webElementGuid>51418d06-7183-450e-9730-428cc9bfcc26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'My Account')]</value>
      <webElementGuid>174c24e5-9cfd-4da1-ad7e-4ea066b1878b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[1]/following::a[1]</value>
      <webElementGuid>4e15fb47-e190-4c95-8656-408d93ea45e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/preceding::a[1]</value>
      <webElementGuid>9feca621-0196-4152-8ee7-d928285ffcce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tutup'])[1]/preceding::a[3]</value>
      <webElementGuid>74bdde48-692e-47ed-8c38-6c0f0b723488</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='My Account']/parent::*</value>
      <webElementGuid>0fdc93ba-bbf1-44e8-9c1f-b3041c8faab3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/dashboard')])[2]</value>
      <webElementGuid>41c5c89d-0b66-4ca9-8c79-ba63a654f678</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/ul/li[3]/a</value>
      <webElementGuid>fb794f89-afe9-4992-bc44-c46ad91b2060</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/dashboard' and (text() = 'My Account
                                                                ' or . = 'My Account
                                                                ')]</value>
      <webElementGuid>2fccd9f3-a366-401f-a06c-7cd43d7eb0d6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
