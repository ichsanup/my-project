<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite Mobile Login with DDT</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>648bf079-30a3-45db-aabe-4b8fe7fd914f</testSuiteGuid>
   <testCaseLink>
      <guid>aacec313-6629-4db4-87af-c2a5c3529907</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/Mob-Log001-Login with DDT</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>56f05a93-05e7-4350-a9c9-42e2b4bb5429</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Mobile Login with DDT</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>56f05a93-05e7-4350-a9c9-42e2b4bb5429</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>8a992229-95c5-4412-814a-047a786386cd</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>56f05a93-05e7-4350-a9c9-42e2b4bb5429</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>59723d66-8cfe-4b35-a1c7-121b5a73b9a1</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
