<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite Change Profile with DDT</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>e0aaad16-6b50-4c09-87f3-d05047b61bcd</testSuiteGuid>
   <testCaseLink>
      <guid>f437692f-6afa-4866-9ba8-842cabae4be8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Upload File/Upload File - DDT</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>59de64ed-58b6-4049-b225-87ca796d7114</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Change Profile with DDT</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>59de64ed-58b6-4049-b225-87ca796d7114</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>path</value>
         <variableId>88f140bb-2429-491b-b088-2c0773b1e45f</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
